package com.epam.cloud.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {

    @GetMapping("/")
    String helloDefault() {
        hello('Greetings from Cloud!')
    }

    @GetMapping("/property")
    String helloFromProperty(@Value('${hello.text}') propertyValue) {
        hello(propertyValue)
    }

    private String hello(value) {
        "${value} © Volodymyr Fastivets, 2021"
    }

}
