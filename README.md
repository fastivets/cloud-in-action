# cloud-in-action

Application technologies and tools:
- Groovy
- Gradle
- Spring Boot
- springdoc-openapi
- Docker

build.sh - builds .jar, docker image and push it to docker hub

buildAndRun.sh - run build.sh and run docker-compose locally

Useful links:
- https://hub.docker.com/repository/docker/fastivets/cloud-in-action

- http://ec2-18-184-5-100.eu-central-1.compute.amazonaws.com/

- http://ec2-18-184-5-100.eu-central-1.compute.amazonaws.com/property

- http://ec2-18-184-5-100.eu-central-1.compute.amazonaws.com/actuator/health

- http://ec2-18-184-5-100.eu-central-1.compute.amazonaws.com/swagger-ui.html

