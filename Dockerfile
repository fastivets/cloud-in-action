FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S demo && adduser -S demo -G demo
USER demo
COPY /build/libs/cloud-in-action-0.0.1-SNAPSHOT.jar cloud-in-action.jar
CMD ["java","-jar","cloud-in-action.jar"]